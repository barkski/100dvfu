# Запуск проекта

A Proof-of-concept of a running Symfony 5 application inside containers
https://gitlab.com/martinpham/symfony-5-docker

```
git clone https://gitlab.com/barkski/100dvfu
cd 100dvfu
cd docker
docker-compose up
После этого перейти на http://localhost
```

# Работа с базой
Чтобы подергать руками базу можно зайти в контейнер и зайти под пользователем:

```
cd docker
docker-compose exec database
mysql -u appuser -papppassword
```

Примеры работы доктриной. База дергается через контейнер php-fpm.
```
!!! Все контейнеры должны быть запущены
 
cd docker
docker-compose exec php-fpm bin/console doctrine:migration:sync-metadata-storage
docker-compose exec php-fpm bin/console make:migration
```

## Миграции и данные
База уже должна быть создана через, но тем не менее команда создания
```
docker-compose run php-fpm bin/console doctrine:database:create
```

После нужно накатить миграции (создание структуры таблиц)
```
docker-compose run php-fpm bin/console doctrine:migration:migrate
```

Потом загрузить фикстуры
```
docker-compose run php-fpm bin/console doctrine:fixtures:load
```

# Авторизация

Авторизация работает через cookie. Чтобы "залогиниться" нужно добавить cookie с ключом "name" со значением <имя пользователя>.
В фикстурах у нас есть следующие имена:
1. Агафоний
2. Марго
3. Мишган

Если кука не указана, берется пользователь с именем "Мишган".