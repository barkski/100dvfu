<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 */
class OrderItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shopOrder;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $priceForOne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    public static function create(
        User $user,
        Order $order,
        string $link,
        string $name,
        int $amount,
        int $priceForOne
    ) {
        $orderItem = new OrderItem();

        $orderItem->setShopOrder($order);
        $orderItem->setUser($user);

        $orderItem->setName($name);
        $orderItem->setLink($link);
        $orderItem->setAmount($amount);
        $orderItem->setPriceForOne($priceForOne);

        return $orderItem;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShopOrder(): ?Order
    {
        return $this->shopOrder;
    }

    public function setShopOrder(?Order $shopOrder): self
    {
        $this->shopOrder = $shopOrder;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriceForOne(): ?int
    {
        return $this->priceForOne;
    }

    public function setPriceForOne(int $priceForOne): self
    {
        $this->priceForOne = $priceForOne;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
