<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Shop::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $organizer;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="shopOrder", orphanRemoval=true, fetch="EAGER")
     */
    private $orderItems;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $pickupAddress;

    /**
     * @ORM\Column(type="integer")
     */
    private $deliveryPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $organizerReward;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public static function create(
        User $user,
        Shop $shop,
        string $name,
        string $description,
        string $pickupAddress,
        int $deliveryPrice,
        int $organizerReward
    ) {
        $order = new Order();

        $order->setOrganizer($user);
        $order->setShop($shop);
       
        $order->setName($name);
        $order->setDescription($description);
        $order->setPickupAddress($pickupAddress);
        $order->setDeliveryPrice($deliveryPrice);
        $order->setOrganizerReward($organizerReward);

        return $order;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getOrganizer(): ?User
    {
        return $this->organizer;
    }

    public function setOrganizer(?User $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    /**
     * Probably we should move this logic to OrderRepository with extra property and specific fetch, but we won't.
     * @return OrderItem[][]
     */
    public function getOrderItemsGroupedByUser(User $firstUser = null): array
    {
        $groupedByUsers = [];

        /** @var OrderItem $orderItem */
        foreach ($this->orderItems as $orderItem) {
            $groupedByUsers[$orderItem->getUser()->getId()]['items'][] = $orderItem;
            $groupedByUsers[$orderItem->getUser()->getId()]['user'] = $orderItem->getUser();
            $groupedByUsers[$orderItem->getUser()->getId()]['price'] =
                ($groupedByUsers[$orderItem->getUser()->getId()]['price'] ?? 0) 
                + $orderItem->getPriceForOne() * $orderItem->getAmount();
        }

        if (!$firstUser) {
            return $groupedByUsers;
        }

        if (!array_key_exists($firstUser->getId(), $groupedByUsers)) {
            return $groupedByUsers;
        }

        $firstItem = [$firstUser->getId() => $groupedByUsers[$firstUser->getId()]];

        unset($groupedByUsers[$firstUser->getId()]);

        return array_merge(
            $firstItem,
            $groupedByUsers
        );
    }

    public function getParticipationPrice()
    {
        $groupedOrderItems = $this->getOrderItemsGroupedByUser();

        $participants = count($groupedOrderItems);

        $price = $this->getDeliveryPrice() + $this->getOrganizerReward();

        if (0 === $participants) {
            $resultPrice = $price;
        } else {
            $resultPrice = $price / $participants;
        }

        // FIXME: временно показываем минимальную сумму в 75 рублей. Ожидаем, что накопится достаточно участников, чтобы еще сильнее снизить.
        return min(75, $resultPrice); 
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setShopOrder($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getShopOrder() === $this) {
                $orderItem->setShopOrder(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPickupAddress(): ?string
    {
        return $this->pickupAddress;
    }

    public function setPickupAddress(string $pickupAddress): self
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    public function getDeliveryPrice(): ?int
    {
        return $this->deliveryPrice;
    }

    public function setDeliveryPrice(int $deliveryPrice): self
    {
        $this->deliveryPrice = $deliveryPrice;

        return $this;
    }

	public function getOrderUsers()
	{
		$items = $this->getOrderItems();
		$uniqueUsers = [];

		foreach ($items as $item) {
			$user = $item->getUser();
			if($user) {
				$uniqueUsers[$user->getId()] = $user;
			}
		}

		return array_values($uniqueUsers);
	}

    public function getOrganizerReward(): ?int
    {
        return $this->organizerReward;
    }

    public function setOrganizerReward(int $organizerReward): self
    {
        $this->organizerReward = $organizerReward;

        return $this;
    }
}
