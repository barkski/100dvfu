<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER_1 = 'Агафоний';
    public const USER_2 = 'Марго';
    public const USER_3 = 'Мишган';
    public const USER_4 = 'Владимир';

    public function load(ObjectManager $manager)
    {
        foreach([self::USER_1, self::USER_2, self::USER_3, self::USER_4] as $name) {
            $user = $this->createUser($name);

            $this->addReference($name, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    private function createUser(string $name): User
    {
        $user = new User();
        $user->setFio($name);

        return $user;
    }
}
