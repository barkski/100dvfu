<?php

namespace App\DataFixtures;

use App\Entity\Shop;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ShopFixtures extends Fixture
{
    public const DEFAULT_SHOP_REFERENCE = 'default-shop';
 
    public function load(ObjectManager $manager)
    {
	    $manager->persist($shop1 = $this->createShop('🍽 Фреш25', 'test1', 'www.fresh25.ru'));
	    $manager->persist($this->createShop('📱 DNS', 'test2', 'www.dns-shop.ru'));
	    $manager->persist($this->createShop('💊 Монастырёв', 'test3', 'xn--80ae2aeeogi5fxc.xn--p1ai'));

	    $manager->flush();

	    $this->addReference(self::DEFAULT_SHOP_REFERENCE, $shop1);
    }

	private function createShop(string $name, string $address, string $catalogLink)
    {
        $shop = new Shop();

        $shop->setName($name);
        $shop->setAddress($address);
        $shop->setLink($catalogLink);

        return $shop;
    }
}
