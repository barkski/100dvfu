<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\OrderItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $order = new Order();

        $order->setName('Name');
        $order->setDescription('Я собираю заказ из фреша');
        $order->setPickupAddress('Корпус 7.1, комната 12');
        $order->setDeliveryPrice(200);
        $order->setOrganizerReward(100);
        $order->setOrganizer($this->getReference(UserFixtures::USER_1));
        $order->setShop($this->getReference(ShopFixtures::DEFAULT_SHOP_REFERENCE));

        $manager->persist($order);

        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_1),
            $order,
            'http://fresh25.ru/dobry_1l',
            'Сок Добрый апельсин 1л.',
            2,
            70
        ));

        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_1),
            $order,
            'http://fresh25.ru/milka',
            'Шоколадка Milka 100г.',
            3,
            50
        ));

        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_2),
            $order,
            'http://fresh25.ru/moloko_1l',
            'Молоко 1 л.',
            1,
            78
        ));
        
        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_2),
            $order,
            'http://fresh25.ru/pyatachok',
            'Сосиски пятачок 1 уп.',
            2,
            120
        ));

        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_3),
            $order,
            'http://fresh25.ru/cucumbersi',
            'Огурцы суражевка 1кг',
            1,
            43
        ));
        
        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_3),
            $order,
            'http://fresh25.ru/mazik',
            'Майонез Провансаль',
            1,
            84
        ));
        
        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_3),
            $order,
            'http://fresh25.ru/tomato_cherry',
            'Помидоры черри',
            1,
            53
        ));
        
        $manager->persist(OrderItem::create(
            $this->getReference(UserFixtures::USER_4),
            $order,
            'http://fresh25.ru/chai_green',
            'Чай зеленый листовой',
            1,
            87
        ));

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ShopFixtures::class,
        ];
    }
}
