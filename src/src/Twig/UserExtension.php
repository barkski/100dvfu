<?php

namespace App\Twig;

use App\Entity\User;
use App\Helper\CurrentUserProvider;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UserExtension extends AbstractExtension
{
    private UserRepository $userRepository;
    private CurrentUserProvider $currentUserProvider;
    private Request $request;

    public function __construct(
        UserRepository $userRepository,
        CurrentUserProvider $currentUserProvider,
        RequestStack $requestStack
    ) {
        $this->userRepository = $userRepository;
        $this->currentUserProvider = $currentUserProvider;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_all_users', [$this, 'getAllUsers']),
            new TwigFunction('get_current_user', [$this, 'getCurrentUser']),
        ];
    }

    /**
     * @return User[]
     */
    public function getAllUsers(): array
    {
        return $this->userRepository->findAll();
    }

    public function getCurrentUser(): User
    {
        return $this->currentUserProvider->getCurrentUser($this->request);
    }
}
