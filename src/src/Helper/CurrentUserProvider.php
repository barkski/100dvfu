<?php

namespace App\Helper;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

class CurrentUserProvider
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getCurrentUser(Request $request): User
    {
        $user = $this->userRepository->findOneBy([
            'id' => $request->cookies->get('user')
        ]) ?? $this->userRepository->findAll()[0];

        return $user;
    }
}
