<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Helper\CurrentUserProvider;
use App\Repository\OrderRepository;
use App\Repository\ShopRepository;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
	/**
	 * Стоимость участия и максимальное количество человек в заказе пока захардкодим
	 */
	private const JOIN_PRICE = 47;

	private const MAX_ORDER_COUNT = 5;

	private CurrentUserProvider $currentUserProvider;
    private ShopRepository $shopRepository;
    private OrderRepository $orderRepository;

    public function __construct(
        CurrentUserProvider $currentUserProvider,
        ShopRepository $shopRepository,
        OrderRepository $orderRepository
    ) {
        $this->currentUserProvider = $currentUserProvider;
        $this->shopRepository = $shopRepository;
        $this->orderRepository = $orderRepository;
    }

	/**
	 * @Route("/orders", name="orders")
	 */
    public function orders(): Response
    {
        $orders = $this->orderRepository->findAll();

	    /**
	     * $maxOrderCount - максимальное количество участников которые могут быть в заказе
	     * Должно браться из ордера
	     */
	    $maxOrderCount = self::MAX_ORDER_COUNT;

        return $this->render('order-list.html.twig', [
            'orders' => $orders,
	        'maxOrderCount' => $maxOrderCount,
        ]);
    }

	/**
	 * @Route("/my-orders", name="my_orders")
	 */
    public function myOrders(Request $request): Response
    {
		$user = $this->currentUserProvider->getCurrentUser($request);

		if (!$user) {
			throw new LogicException('Current user is not logged.');
		}

        $orders = $this->orderRepository->findBy(['organizer' => $user]);

	    /**
	     * $maxOrderCount - максимальное количество участников которые могут быть в заказе
	     * Должно браться из ордера
	     */
	    $maxOrderCount = self::MAX_ORDER_COUNT;

        return $this->render('order-list.html.twig', [
            'orders' => $orders,
	        'maxOrderCount' => $maxOrderCount,
        ]);
    }

	/**
	 * @Route("/order/{id<\d+>}", name="order")
	 */
	public function order(int $id): Response
	{
        $order = $this->orderRepository->findOneBy(['id' => $id]);

		/**
		 * $maxOrderCount - максимальное количество участников которые могут быть в заказе
		 * Должно браться из ордера
		 */
		$maxOrderCount = self::MAX_ORDER_COUNT;

		return $this->render('order.html.twig', [
			'order' => $order,
			'memberCount' => count($order->getOrderUsers()),
			'maxOrderCount' => $maxOrderCount,
		]);
	}

	/**
	 * @Route("/order/{id<\d+>}/add-item", methods={"POST"})
	 */
	public function postAddOrderItem(Request $request, int $id): Response
	{
		$parameters = $request->request->all();

        $order = $this->orderRepository->findOneBy(['id' => $id]);

        $newOrderItem = OrderItem::create(
            $this->currentUserProvider->getCurrentUser($request),
            $order,
            $parameters['link'],
            $parameters['name'],
            $parameters['amount'],
            $parameters['price'],
        );

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($newOrderItem);
        $entityManager->flush();

		return $this->redirectToRoute('order', [
			'id' => $order->getId(),
		]);
	}

	/**
	 * @Route("/order/{id<\d+>}/add-item", name="order-add-item", methods={"GET"})
	 */
	public function showAddOrderItem(int $id): Response
	{
		$order = $this->orderRepository->findOneBy(['id' => $id]);

		return $this->render('add-order-item.html.twig', [
			'order' => $order
		]);
	}


	/**
	 * @Route("/order/{shopId<\d+>}/add-order", methods={"GET"}, name="new_order")
	 */
	public function addOrder(int $shopId): Response
	{
		return $this->render('add-order.html.twig');
	}

    /**
     * @Route("/order/{shopId<\d+>}/add-order", methods={"POST"})
     */
    public function postOrder(Request $request, int $shopId): Response
    {
        $parameters = $request->request->all();

        $newOrder = Order::create(
            $this->currentUserProvider->getCurrentUser($request),
            $this->shopRepository->findOneBy(['id' => $shopId]),
            $parameters['name'],
            $parameters['description'],
            $parameters['pickup-address'],
            $parameters['delivery-price'],
            $parameters['reward'],
        );

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($newOrder);
        $entityManager->flush();

        return $this->redirectToRoute('order', [
            'id' => $newOrder->getId(),
        ]);
    }
}
