<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/login/{userId}", name="admin_login")
     */
    public function loginUser(Request $request, int $userId): Response
    {
        $response = $this->redirect($request->headers->get('referer'));

        $response->headers->setCookie(new Cookie('user', $userId));

        return $response;
    }
}
