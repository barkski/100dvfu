<?php

namespace App\Controller;

use App\Repository\ShopRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopListController extends AbstractController
{
	private ShopRepository $shopRepository;

	public function __construct(ShopRepository $shopRepository)
	{
		$this->shopRepository = $shopRepository;
	}

	/**
	 * @Route("/shop-list", name="shop-list")
	 */
	public function shopList(): Response
	{
		$shops = $this->shopRepository->findAll();

		return $this->render('shop-list.html.twig', [
			'shops' => $shops,
		]);
	}
}